import os
import requests
from flask import Flask, json

#Requesting gitlab version with my API token
gitlab_token= {"PRIVATE-TOKEN": os.getenv('API_TOKEN')}
gitlab_content=requests.get("https://gitlab.com/api/v4/version", headers=gitlab_token).content

#Formating to only show version to the user
gitlab_dic=json.loads(gitlab_content)
gitlab_version=gitlab_dic['version']
gitlab_version_in_json_format={'version':gitlab_version}


api = Flask(__name__)

@api.route('/version', methods=['GET'])
def get_gitlab_version():
  return json.dumps(gitlab_version_in_json_format)

if __name__ == '__main__':
    #Run the server and expose it to the LAN
    api.run(host='0.0.0.0', port=os.getenv('PORT')) 
